# Site web [Pony France](https://ponyfrane.net/)
![MIT license](http://img.shields.io/badge/license-MIT-brightgreen.svg)](http://pf.mit-license.org/)

### Compiler le style SASS vers CSS
```
gem install sass # Si ce n'est pas fait
# Soyez bien à la racine du projet
sass --update assets/style.scss:assets/style.css --no-cache --style compressed
```

### Licence
[MIT license (pf.mit-license.org)](http://pf.mit-license.org/)

### Sources images
- `assets/derpy-hankofficier.svg` utilisé en fond de page est un vecteur de Hank Officier. (Impossible de retrouver la source ?)
- `assets/twilight.png` utilisé dans la [404](https://ponyfrance.net/404.html)a été dessinée par [XNightMelody](http://xnightmelody.deviantart.com/) ([Source](http://xnightmelody.deviantart.com/art/Twilight-384211758))
