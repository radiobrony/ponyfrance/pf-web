<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Pony France Network</title>
	<meta name="description" content="Le réseau Pony France est un réseau entre amis de divers sites de la communauté Brony Francophone">
	<link rel="icon" type="image/png" href="assets/128.png">
	<link rel="shortcut icon" type="image/png" href="assets/128.png">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="assets/style.css">
</head>
<body>
	<script src="https://ponyfrance.net/i/pony-france.js"></script>

	<!--[if lt IE 7]>
	<p class="browsehappy">Vous utilisez un navigateur <strong>trop ancien</strong>. Merci de <a href="http://browsehappy.com/">mettre à jour votre navigateur</a>.</p>
	<![endif]-->

	<div id="logo">
		<section>
			<img class="logo" src="assets/logo.jpg" alt="Pony France">
			<h1>Pony France</h1>
			<div class="links">
				<a href="http://status.ponyfrance.net/">Status <img src="assets/statuscake.png"></a><br>
				<a href="https://twitter.com/PonyFrance">Twitter <img src="assets/twitter.png"></a>
			</div>
		</section>
	</div>

	<div id="sites">
		<section>
			<h2>Les sites du réseau Pony France</h2>
			<ul>
				<li>
					<h3>Radio Brony</h3>
					<p>
						Radio Brony est la première radio francophone ayant pour thème la série My Little Pony: Friendship is Magic. Elle diffuse de <b>la musique 24/7 avec des émissions régulières</b> ! Retrouvez les liens des émissions sur notre page "Live" et rejoignez le chat.
						<br><br><span class="link"><a href="https://www.radiobrony.fr/">Aller sur Radio Brony</a></span>
					</p>
				</li>
				<li>
					<h3>New Lunar Republic</h3>
					<p>
						Envie de (re)voir vos épisodes préférés de la série ou les films ou même de lire les comics ? La New Lunar Republic a ce qu'il vous faut en version originale (VO), sous-titré (VostFR) et même en version française (VF) <b>disponible en streaming</b> !
						<br><br><span class="link"><a href="https://www.newlunarrepublic.fr/">Aller sur New Lunar Republic</a></span>
					</p>
				</li>

				<li>
					<h3>Canterlot Comics</h3>
					<p>
						Canterlot Comics est le site de recensement de comics du fandom, et ce en français, anglais et plus encore grâce aux traductions ! Mis à jour très régulièrement, les comics sont organisés en plusieurs catégories, <b>le tout avec une navigation rapide, aisée et sans publicité</b>.
						<br><br><span class="link"><a href="http://www.canterlotcomics.com/">Aller sur Canterlot Comics</a></span>
					</p>
				</li>
				<li>
					<h3>MLP Fictions</h3>
					<p>
						MLP Fictions <b>était</b> le site de partage de fan-fictions sur la série My Little Pony: Friendship is Magic. Retrouvez-y de très nombreuses fictions qui ne sont <b>écrits et traduits uniquement en français</b> ! <i>Romance</i>, <i>Slice of life</i>, <i>Polar</i> et encore, il en a de tout genre à lire et découvrir sur MLP Fictions !
						<br><br><span class="link"><a href="https://mlpfictions.com/">Aller sur MLP Fictions</a></span>
					</p>
				</li>
				<li>
					<h3>Equestria Social Network</h3>
					<p>
						Equestria.Social est <b>une instance Mastodon pour tous les fans de poneys</b>, créée pour fédérer les Bronies et Pegasisters voulant rejoindre le "fediverse", un énorme réseau social disponible à travers plusieurs domaines ! Les inscriptions sont ouvertes à tous, même non-bronies !
						<br><br><span class="link"><a href="https://equestria.social/">Aller sur Equestria Social Network</a></span>
					</p>
				</li>
<!--
				<li>
					<h3>MLP CCG .Fr</h3>
					<p>
						MLP CCG .Fr est fait par et pour la communauté de fans sur le thème du jeu de cartes <i>My Little Pony Collectible Card Game™</i> appartenant à Hasbro et produit par Enterplay.
						<br><br><span class="link"><a href="https://mlpccg.fr/">Aller sur MLP CCG .Fr</a></span>
					</p>
				</li>

				<li>
					<h3>Le Coin Brony</h3>
					<p>
						Le Coin Brony est un groupe qui présente des sous-titres en français sur diverses créations du fandom, de la <i>Bronalyse</i> ainsi qu'une émission de débat nommé <i>Grandes Gueules de Bronies</i>.
						<br><br><span class="link"><a href="https://www.facebook.com/LeCoinBrony/">Aller sur Le Coin Brony</a></span>
					</p>
				</li>
				<li>
					<h3>BroniesQC (MLP Québec)</h3>
					<p>
						BroniesQC est un site dédié à la communauté Brony du Québec. Vous pouvez y retrouver tout ce qui se passe dans la province : news, memes, art, fics et plus encore.
						<br><br><span class="link"><a href="https://mlp.quebec/">Aller sur MLP Québec</a></span>
					</p>
				</li>


					<h3>Unipop'Corn</h3>
					<p>
						Unipop'Corn est un meet-up mensuel qui a pour but de <b>rassembler tous geeks et otaks' pour passer un bon moment !</b> Les meet-ups sont organisés à travers la France et parfois même en Belgique, Suisse et Québec pour une superbe journée avec des films, une bouffe et pleins de rencontres !
						<br><br><span class="link"><a href="http://unipopcorn.org">Aller sur Unipop'Corn</a></span>
					</p>
				</li>
				<li>
					<h3>Ponies Holidays</h3>
					<p>
						Vous n'avez rien de prévu pour les grandes vacances ? Envie de passer un bon moment entre Bronies ? Préparez-vous à réserver vos semaines car Ponies Holidays vous propose de <b>partir en vacances ou en activités, tous entre fans de la série, pour une grosse barre du fun !</b>
						<br><br><span class="link"><a href="http://www.poniesholidays.org/">Aller sur Ponies Holidays</a></span>
					</p>
				</li>
-->
				<li>
					<h3>Et plus encore...</h3>
					<p>
						Retrouvez tout le monde dans la barre en haut du site ! Sachez que nous hébergeons aussi d'autres choses que du <i>Poney</i> et plusieurs sites personnels.
					</p>
				</li>
			</ul>
		</section>

	</div>

	<div id="info">
		<section>
			<h2>Mais qu’est-ce que Pony France ?</h2>
			<p>
				Nous sommes un réseau lancé au départ entre amis, et c'est cette barre qui se trouve en haut de nos sites qui marque notre lien. C'est un peu un moyen d'afficher notre union et de vous proposer d'autres sites avec qui nous avons confiance !
			</p>
		</section>
		<section>
			<h2>Tout les sites sont-ils hébergés chez vous ?</h2>
			<p>
				Non, pas tous. Les propriétaires peuvent garder leur site sur leurs serveurs. On va pas en faire tout un foin pour tout avoir chez nous !
			</p>
		</section>
		<section>
			<h2>Avez-vous un compte sur un réseau social ?</h2>
			<p>
				Nous avons un compte Twitter qui peut annoncer quelques maintenances et autre, <a href="https://twitter.com/PonyFrance" target="_blank">@PonyFrance</a>.
			</p>
		</section>
	</div>

	<div id="host">
		<section>
			<h2>Service d'hébergement</h2>
			<p>
				Vous avez un projet de site web et vous cherchez où pouvoir l'héberger ? Nous pouvons vous aider !<br>
				Nous vous proposons un hébergement web (NGinx + PHP + MySQL) avec un accès SSH basique pour la gestion de vos fichier et l'utilisation de GIT.<br>
				Vous pouvez demander plus d'informations en contactant Kody par email <code>ponyfrance@kdy.ch</code> ou Twitter <span class="link"><a href="https://twitter.com/0kody">@0kody</a></span>.
			</p>
			<p>
				<b>Sont acceptés :</b> tout projets avec des personnes motivés, que cela soit Brony ou non.<br>
				<b>Ne sont pas acceptés :</b> forums, serveurs et sites de serveurs de jeu, sites d'hébergement de gros fichiers et tout contenu explicite.
			</p>
		</section>
	</div>

	<div id="quote">
		<section>
			<p class="quote">
			<?php include 'quote.php'; ?>
			</p>
			<a data-scroll href="#pony-france"><img class="derpy" src="assets/derpy-hankofficier.svg" alt="Remonter !"></a>
		</section>
	</div>
	<script src="assets/smooth.js"></script>
	<script>smoothScroll.init();</script>
</body>
</html>
